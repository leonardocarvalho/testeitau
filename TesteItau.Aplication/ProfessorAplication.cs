﻿using System;
using System.Collections.Generic;
using System.Text;
using TesteItau.Domain.Entities;
using TesteItau.Infra.Repositories;

namespace TesteItau.Aplication
{
    public class ProfessorAplication
    {
        public readonly ProfessorRepository _professorRepository;
        public readonly ProfessorMateriaRepository _professorMateriaRepository;
        public readonly MateriaRepository _materiaRepository;

        public ProfessorAplication(ProfessorRepository professorRepository, MateriaRepository materiaRepository, ProfessorMateriaRepository professorMateriaRepository)
        {
            _professorRepository = professorRepository;
            _materiaRepository = materiaRepository;
            _professorMateriaRepository = professorMateriaRepository;
        }

        public void VincularProfessorMateria(ProfessorMateria professorMateria)
        {
            var materia = _materiaRepository.GetMateria(professorMateria.MateriaId);
            if (materia == null)
            {
                throw new Exception("materia não existe");
            }
            var professor = _professorRepository.GetProfessor(professorMateria.ProfessorId);
            if (professor == null)
            {
                throw new Exception("professor não existe");
            }

            _professorMateriaRepository.Save(professorMateria);

        }
    }
}
