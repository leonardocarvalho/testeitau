﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TesteItau.Domain.DTOs;
using TesteItau.Domain.Entities;
using TesteItau.Infra.Repositories;

namespace TesteItau.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MateriaController : ControllerBase
    {
        public readonly MateriaRepository _materiaRepository;
        public readonly IMapper _mapper;

        public MateriaController(MateriaRepository materiaRepository, IMapper mapper)
        {
            _materiaRepository = materiaRepository;
            _mapper = mapper;
        }

        // GET: api/Materia
        [HttpGet]
        public List<Materia> Get()
        {
            var retorno = _materiaRepository.GetMaterias();
            return retorno;
        }

        // GET: api/Materia/5
        [HttpGet("{id}")]
        public Materia Get(int id)
        {
            return _materiaRepository.GetMateria(id);
        }

        // POST: api/Materia
        [HttpPost]
        public void Post([FromBody] MateriaDTO materia)
        {
            _materiaRepository.Save(_mapper.Map<Materia>(materia));
        }

        // PUT: api/Materia/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Materia materia)
        {
            _materiaRepository.Update(materia);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var materia = _materiaRepository.GetMateria(id);
            _materiaRepository.Delete(materia);
        }
    }
}
