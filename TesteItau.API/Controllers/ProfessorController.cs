﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TesteItau.Aplication;
using TesteItau.Domain.DTOs;
using TesteItau.Domain.Entities;
using TesteItau.Infra.Repositories;

namespace TesteItau.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfessorController : ControllerBase
    {
        public readonly ProfessorRepository _professorRepository;
        public readonly ProfessorAplication _professorAplication;
        public readonly IMapper _mapper;
        public ProfessorController(ProfessorRepository professorRepository, ProfessorAplication professorAplication, IMapper mapper)
        {
            _professorRepository = professorRepository;
            _professorAplication = professorAplication;
            _mapper = mapper;
        }

        // GET: api/Professor
        [HttpGet]
        public List<Professor> Get()
        {
            return _professorRepository.GetProfessores();
        }

        // GET: api/Professor/5
        [HttpGet("{id}")]
        public Professor Get(int id)
        {
            return _professorRepository.GetProfessor(id);
        }

        // POST: api/Professor
        [HttpPost]
        public void Post([FromBody] ProfessorDTO professor)
        {
            _professorRepository.Save(_mapper.Map<Professor>(professor));
        }

        // PUT: api/Professor/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] ProfessorDTO professorDTO)
        {
            var professor = _mapper.Map<Professor>(professorDTO);
            professor.Id = id;
            _professorRepository.Update(professor);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var professor = _professorRepository.GetProfessor(id);
            _professorRepository.Delete(professor);
        }

        [HttpPut("vincularProfessorMateria")]
        public IActionResult VincularProfessorMateria(ProfessorMateriaDTO professorMateria)
        {
            try
            {
                _professorAplication.VincularProfessorMateria(_mapper.Map<ProfessorMateria>(professorMateria));
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }

        }
    }
}
