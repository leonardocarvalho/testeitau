﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TesteItau.Domain.DTOs
{
    public class ProfessorDTO
    {
        public string Nome { get; set; }
        public string CPF { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Sexo { get; set; }
    }
}
