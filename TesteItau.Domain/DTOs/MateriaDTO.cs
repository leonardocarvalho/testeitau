﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TesteItau.Domain.DTOs
{
    public class MateriaDTO
    {
        public string Nome { get; set; }
        public string Descricao { get; set; }
    }
}
