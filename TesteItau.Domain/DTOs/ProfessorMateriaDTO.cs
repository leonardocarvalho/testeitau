﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TesteItau.Domain.DTOs
{
    public class ProfessorMateriaDTO
    {
        public int ProfessorId { get; set; }
        public int MateriaId { get; set; }
    }
}
