﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TesteItau.Domain.Entities
{
    public class Materia : BaseEntity
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public List<ProfessorMateria> ProfessorMaterias { get; set; }
    }
}
