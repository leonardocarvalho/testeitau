﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TesteItau.Domain.Entities
{
    public class Professor : BaseEntity
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Sexo { get; set; }
        public List<ProfessorMateria> ProfessorMaterias { get; set; }
    }
}
