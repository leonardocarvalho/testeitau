﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TesteItau.Domain.Entities
{
    public class ProfessorMateria : BaseEntity
    {
        public int ProfessorId { get; set; }
        public int MateriaId { get; set; }
        public Materia Materia { get; set; }
        public Professor Professor { get; set; }
    }
}
