﻿using System;
using System.Collections.Generic;
using System.Text;
using TesteItau.Domain.Entities;
using TesteItau.Infra.Database;

namespace TesteItau.Infra.Repositories
{
    public class BaseRepository<T> where T : BaseEntity
    {
        protected readonly DatabaseContext dbContext;

        public BaseRepository(DatabaseContext databaseContext)
        {
            dbContext = databaseContext;
        }

        public void Save(T obj)
        {
            dbContext.Set<T>().Add(obj);
            dbContext.SaveChanges();
        }
        public void Update(T obj)
        {
            dbContext.Set<T>().Update(obj);
            dbContext.SaveChanges();
        }

        public void Delete(T obj)
        {
            dbContext.Set<T>().Remove(obj);
            dbContext.SaveChanges();
        }
    }
}
