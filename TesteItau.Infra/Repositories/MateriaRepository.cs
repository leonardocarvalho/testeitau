﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TesteItau.Domain.Entities;
using TesteItau.Infra.Database;

namespace TesteItau.Infra.Repositories
{
    public class MateriaRepository : BaseRepository<Materia>
    {
        public MateriaRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }

        public List<Materia> GetMaterias()
        {
            return dbContext.Materias.Include(x => x.ProfessorMaterias).ThenInclude(x => x.Professor).ToList();
        }

        public Materia GetMateria(int id)
        {
            return dbContext.Materias.Include(x => x.ProfessorMaterias).ThenInclude(x => x.Professor).FirstOrDefault(x => x.Id == id);
        }
    }
}
