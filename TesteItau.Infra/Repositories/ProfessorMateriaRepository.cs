﻿using System;
using System.Collections.Generic;
using System.Text;
using TesteItau.Domain.Entities;
using TesteItau.Infra.Database;

namespace TesteItau.Infra.Repositories
{
    public class ProfessorMateriaRepository : BaseRepository<ProfessorMateria>
    {
        public ProfessorMateriaRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {

        }
    }
}
