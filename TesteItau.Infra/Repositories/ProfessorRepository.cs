﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TesteItau.Domain.Entities;
using TesteItau.Infra.Database;

namespace TesteItau.Infra.Repositories
{
    public class ProfessorRepository : BaseRepository<Professor>
    {
        public ProfessorRepository(DatabaseContext databaseContext)
            : base(databaseContext)
        {
        }

        public List<Professor> GetProfessores()
        {
            return dbContext.Professores.Include(x => x.ProfessorMaterias).ThenInclude(x => x.Materia).ToList();
        }

        public Professor GetProfessor(int id)
        {
            return dbContext.Professores.Include(x => x.ProfessorMaterias).ThenInclude(x => x.Materia).FirstOrDefault(x => x.Id == id);
        }
    }
}
