﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TesteItau.Domain.Entities;

namespace TesteItau.Infra.Configuration
{
    public class ProfessorMateriaConfiguration : IEntityTypeConfiguration<ProfessorMateria>
    {
        public void Configure(EntityTypeBuilder<ProfessorMateria> builder)
        {
            builder.HasKey(x => new { x.MateriaId, x.ProfessorId});

        }
    }
}
